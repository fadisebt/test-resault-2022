<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'pro_id',
        'modifier',
    ];

    public function products()
    {
        return $this->hasMany(Unit::class);
    }
}
