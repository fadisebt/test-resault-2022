<?php

namespace App\Models;
use App\Models\Unit_prodact;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        //
    }

    public function getTotalQuantityAttribute()
    {
         $a=intval( Unit_prodact::where('product_id',$this->id)->sum('amount'));
        return $a ;
    }

    public function getImagePathAttribute()
    {
        $path=Image::where('o_id',$this->id)->value('path');
        return $path;
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }
    public function unit()
    {
        return $this->hasMany(Unit_prodact::class);
    }

}
