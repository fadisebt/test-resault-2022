<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'path',
        'description',
        'o_id',
        'o_type',
        'type',
    ];
    public function user()
    {
        return $this->belongsTo(Product::class);
    }
    public function image()
    {
        return $this->belongsTo(User::class);
    }
}
