<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit_prodact extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'unit_id',
        'amount'
    ];

    public function product()
    {
        return $this->belongsTo(Prodact::class,'product_id');
    }
    public function unit()
    {
        return $this->belongsTo(Unit::class,'unit_id');
    }
}
