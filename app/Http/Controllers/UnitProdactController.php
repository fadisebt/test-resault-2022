<?php

namespace App\Http\Controllers;
use App\Models\Unit_prodact;
use Illuminate\Http\Request;

class UnitProdactController extends Controller
{
    //
     public function add_unit_product(Request $request)
     {
        $unit_pro = $request->validate([
            'product_id'=>'required|integer',
            'unit_id'=>'required|integer',
            'amount'=>'required|integer'
        ]);
        $unit_product=Unit_prodact::create([
            'product_id'=>$unit_pro['product_id'],
            'unit_id'=>$unit_pro['unit_id'],
            'amount'=>$unit_pro['amount']
        ]
        );
        return response()->json(TRUE);
     }
}
