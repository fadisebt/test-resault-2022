<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Unit;
use App\Models\Unit_prodact;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function add(Request $request)
    {
        try{

            $product = $request->validate([
                'name' => 'required|string'
            ]);

            $pro=Product::create([
                'name'=>$product['name']
            ]);

            return response()->json('add product successfuly');
        }catch (\Exception $e) {
            return response()->json(['state' => false,
            'massge'=>$e->getMessage()
        ]);
        }
    }


    public function ProductShow(Request $request)
    {
        try{

            $product=Product::where('id',$request)->first();
            $unit=Unit::where('name',$request)->first();
            return response()->json([
                'Product'=>$product->name,
                'total_quantity'=>$unit->modifier
            ]);

        }catch (\Exception $e) {
            return response()->json(['state' => false,
            'massge'=>$e->getMassge()
        ]);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
      $product=Product::where('id',$product_id)->first();
      return response()->json($product['total_quantity']);
    }
////////////////////
    public function show_p_u($product_id,$unit_id)
    {
      $product=Product::where('id',$product_id)->first();
      $unit_product=Unit_prodact::where('product_id',$product_id)->where('unit_id',$unit_id)->sum('amount');
      $product['total_quantity']=$unit_product;
      return response()->json($product['total_quantity']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
