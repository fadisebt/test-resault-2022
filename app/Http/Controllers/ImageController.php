<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    public function add_image(Request $request)
    {
        try {
            $data = $request->validate([
              'o_id' => 'required|integer',
              'o_type' => 'required|string',
              'path' => 'required|file|nullable',
              'description'=>'required|string'
            ]);
            $path = $request->file('path');
            $filename = time().$path->getClientOriginalName();
            Storage::disk('public')->put($filename, File::get($path));
            Image::insert([
              'o_id' => $data['o_id'],
              'o_type' => $data['o_type'],
              'path' => $filename,
              'type' => $request->path->getClientMimeType(),
              'description'=>$data['description']
            ]);
          } catch (\Exception $e) {
            return response('error');
          }
          $d=$request->path->getClientMimeType();
          return response()->json([
            'state'=>true,
            'massage'=>'the image add successfuly'
          ]);
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $data=Image::where('o_id',$request)->first();
        if($data->o_type == 'user')
        {
            $user = User::find($request);
            return response()->json([
                'path'=>$user['image_path']
            ]);
        }
        if($data->o_type == 'product')
        {
            $product = Product::find($request);
            return response()->json([
                'path'=> $product['image_path']
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
