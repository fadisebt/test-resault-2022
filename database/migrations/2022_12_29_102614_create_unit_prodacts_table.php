<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_prodacts', function (Blueprint $table) {
            $table->Integer('product_id')->unsigned();
            $table->Integer('unit_id')->unsigned();
            $table->Integer('amount')->unsigned();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')
				->onDelete('cascade')
				->onUpdate('restrict');
            $table->foreign('unit_id')->references('id')->on('units')
				->onDelete('cascade')
				->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_prodacts');
    }
};
