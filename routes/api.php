<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UnitController;
use Illuminate\Http\Request;
use App\Http\Controllers\UnitProdactController;
use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(ProductController::class)->group(function()
{
    Route::post('/add', 'add');
    Route::get('product/{product_id}', 'show');
    Route::get('product/{product_id}/unit_id={unit_id}', 'show_p_u');
});

Route::controller(UnitController::class)->group(function()
{
    Route::post('/add_unit', 'add_unit');
});
Route::controller(UnitProdactController::class)->group(function()
{
    Route::post('/add_unit_product', 'add_unit_product');
});
Route::controller(ImageController::class)->group(function()
{
    Route::post('/add_image', 'add_image');
    Route::get('products/{product_id}', 'store');
    Route::get('users/{user_id}', 'store');
});

